# Server

Un semplice server scritto in NodeJS che esponde delle web api di gestione di un utente e salva i dati in un file di testo in formato JSON

## Prerequisiti

- [Visual Studio Code](https://code.visualstudio.com/download)
- [NodeJS](https://nodejs.org)

## Installazione

- `npm install`

## Esecuzione del server

- `npm start`

## Esempio di utilizzo

- Aprire il browser su `http://localhost:3030/`

## Problemi

Se il server rimane attivo anche dopo la chiusura

NB: {PID} è una variabile che indica l'id del processo da uccidere, che verrà restituito dal primo comando

- Linux/Mac OS: `lsof -i tcp:3030` e `kill -9 {PID}` 
- Windows: `netstat -ano | findstr :3030` e `tskill {PID}`
